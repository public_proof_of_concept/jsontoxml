<#assign msg = body?eval_json>
<?xml version="1.0" encoding="UTF-8"?>
<book>
    <title>${msg.book.title}</title>
    <authors>
        <#list msg.book.authors as author>
        <author>
            <name>${author.name}</name>
            <lastName>${author.lastName}</lastName>
        </author>
        </#list>
    </authors>
    <isbn>${msg.book.isbn}</isbn>
    <edition>${msg.book.edition}</edition>
</book>