package com.nuuptech.jsontoxml.routes;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.servlet.CamelHttpTransportServlet;
import org.apache.camel.model.rest.RestParamType;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class Rest extends RouteBuilder {

    @Bean
    ServletRegistrationBean servletRegistrationBean() {
        ServletRegistrationBean servlet = new ServletRegistrationBean
                (new CamelHttpTransportServlet(), "/camel/*");
        servlet.setName("CamelServlet");
        return servlet;
    }

    public static final String REQUEST = "{"
            + " \"book\": {"
            + "   \"title\": \"100 años de soledad\","
            + "   \"authors\": ["
            + "     {"
            + "       \"name\": \"Gabriel\","
            + "       \"lastName\": \"Garcia Marquez\""
            + "     }"
            + "   ],"
            + "   \"isbn\": \"9780060929794\","
            + "   \"edition\": \"1\""
            + " }"
            + "}";

    public static final String RESPONSE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            + "<book>"
            + "    <title>100 años de soledad</title>"
            + "    <authors>"
            + "        <author>"
            + "            <name>Gabriel</name>"
            + "            <lastName>Garcia Marquez</lastName>"
            + "        </author>"
            + "    </authors>"
            + "    <isbn>9780060929794</isbn>"
            + "    <edition1></edition>"
            + "</book>";

    @Override
    public void configure() throws Exception {

        rest("/")
            .description("Transform JSON request to XML")
            .post("transform")
            .consumes(MediaType.APPLICATION_JSON_VALUE)
            .param()
                .name("body")
                .required(true)
                .type(RestParamType.body)
                .example(MediaType.APPLICATION_JSON.toString(), REQUEST)
            .endParam()
            .produces(MediaType.APPLICATION_XML_VALUE)
            .responseMessage()
                .code(HttpStatus.OK.value())
                .message("When everything goes well.")
                .example("body", RESPONSE)
            .endResponseMessage()
            .route()
                .routeId("postRoute")
                .convertBodyTo(String.class)
                .to("json-validator:classpath:ValidationSchema.json?contentCache=true")
                .to("freemarker:classpath:XmlTransformation.ftl?contentCache=true")
                .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_XML_VALUE))
            .end();
    }

}
